#PSShare

##iOS5
For Facebook integration add this values to your .plist:

    <key>CFBundleURLTypes</key>
    <array>
      <dict>
        <key>CFBundleURLSchemes</key>
        <array>
          <string>fbYOUR_FACEBOOK_APP_ID</string>
        </array>
      </dict>
    </array>
    <key>FacebookAppID</key>
    <string>YOUR_FACEBOOK_APP_ID</string>

And this method to your app delegate:

    #AppDelegate.m
    - (BOOL)application:(UIApplication *)application
                openURL:(NSURL *)url
      sourceApplication:(NSString *)sourceApplication
             annotation:(id)annotation
    {
        return [FBSession.activeSession handleOpenURL:url];
    }