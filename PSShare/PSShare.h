//
//  PSShare.h
//  PSShare
//
//  Created by Felipe Vieira on 11/05/12.
//  Copyright (c) 2012 Pinuts Studios Cons. em T.I. Ltda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Social/Social.h>
#import <Twitter/Twitter.h>
#import <MessageUI/MessageUI.h>

@class PSShare;

@protocol PSShareDelegate <NSObject>

#pragma mark Twitter delegate methods
- (void)psShareDidPostMessageOnTwitterWithSuccess:(PSShare *)psShare;
- (void)psShareDidNotPostMessageOnTwitter:(PSShare *)psShare;
#pragma mark Mail delegate methods
- (void)psShareDidSendMailWithSuccess:(PSShare *)psShare;
- (void)psShareDidNotSendMail:(PSShare *)psShare;
#pragma mark Facebook delegate methods
- (void)psShareDidPublishPostOnFacebookWithSuccess:(PSShare *)psShare;
- (void)psShareDidNotPublishPostOnFacebook:(PSShare *)psShare;
@end

@interface PSShare : NSObject <MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) id <PSShareDelegate> delegate;

- (id)initWithDelegate:(id<PSShareDelegate>)delegate;

- (UIViewController *)twitterShareComposeViewControllerForMessage:(NSString *)message;
- (UIViewController *)mailShareComposeViewControllerForMessage:(NSString *)message withTitle:(NSString *)title;
- (UIViewController *)facebookShareComposeViewControllerForWallPost:(NSDictionary *)wallPost;

@end