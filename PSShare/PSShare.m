//
//  PSShare.m
//  PSShare
//
//  Created by Felipe Vieira on 11/12/12.
//  Copyright (c) 2012 Pinuts Studios Cons. em T.I. Ltda. All rights reserved.
//

#import "PSShare.h"
#import <FacebookSDK/FacebookSDK.h>
#import "DEFacebookComposeViewController.h"

@implementation PSShare

- (id)initWithDelegate:(id<PSShareDelegate>)delegate {
    self = [super init];

    if (self) {
        self.delegate = delegate;
    }

    return self;
}

#pragma mark - Share methods
#pragma mark Twitter methods

- (UIViewController *)twitterShareComposeViewControllerForMessage:(NSString *)message {

    if ([SLComposeViewController class]) {
        return [self iOS6TwitterShareComposeViewControllerForMessage:message];
    } else {
        return [self iOS5TwitterShareComposeViewControllerForMessage:message];
    }
}

- (UIViewController *)iOS6TwitterShareComposeViewControllerForMessage:(NSString *)message {
    SLComposeViewController *slTwitterComposeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    [slTwitterComposeViewController setInitialText:message];
    [slTwitterComposeViewController setCompletionHandler:^(SLComposeViewControllerResult result) {
        switch (result) {
            case SLComposeViewControllerResultDone:
                [self.delegate psShareDidPostMessageOnTwitterWithSuccess:self];
                break;
            case SLComposeViewControllerResultCancelled:
                [self.delegate psShareDidNotPostMessageOnTwitter:self];
                break;
        }
    }];

    return slTwitterComposeViewController;
}

- (UIViewController *)iOS5TwitterShareComposeViewControllerForMessage:(NSString *)message {
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
    TWTweetComposeViewController *tweetComposeViewController = [[TWTweetComposeViewController alloc] init];
    [tweetComposeViewController setInitialText:message];
    [tweetComposeViewController setCompletionHandler:^(TWTweetComposeViewControllerResult result) {
        switch (result) {
            case TWTweetComposeViewControllerResultDone:
                [self.delegate psShareDidPostMessageOnTwitterWithSuccess:self];
                break;
            case TWTweetComposeViewControllerResultCancelled:
                [self.delegate psShareDidNotPostMessageOnTwitter:self];
                break;
        }
    }];

    return tweetComposeViewController;
}

#pragma mark Facebook methods

/*
 NSDictionary *wallPost = @{ @"link":@"some link",
 @"picture":@"url for some image",
 @"name":@"title for the post",
 @"caption":@"caption",
 @"message":@"THE post" }
 */
- (UIViewController *)facebookShareComposeViewControllerForWallPost:(NSDictionary *)wallPost {
    if ([SLComposeViewController class]) {
        return [self iOS6FacebookShareComposeViewControllerForWallPost:wallPost];
    } else {
        BOOL allowLoginUI = ![self facebookAuthenticated];
        [self openSessionWithAllowLoginUI:allowLoginUI];
        return [self iOS5FacebookShareComposeViewControllerForWallPost:wallPost];
    }
}

- (UIViewController *)iOS6FacebookShareComposeViewControllerForWallPost:(NSDictionary *)wallPost {
    SLComposeViewController *slFacebookComposeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [slFacebookComposeViewController setInitialText:[wallPost objectForKey:@"message"]];
    [slFacebookComposeViewController addURL:[NSURL URLWithString:[wallPost objectForKey:@"link"]]];
    [slFacebookComposeViewController setCompletionHandler:^(SLComposeViewControllerResult result) {
        switch (result) {
            case SLComposeViewControllerResultDone:
                [self.delegate psShareDidPublishPostOnFacebookWithSuccess:self];
                break;
            case SLComposeViewControllerResultCancelled:
                [self.delegate psShareDidNotPublishPostOnFacebook:self];
                break;
        }
    }];

    return slFacebookComposeViewController;
}

- (UIViewController *)iOS5FacebookShareComposeViewControllerForWallPost:(NSDictionary *)wallPost {
    DEFacebookComposeViewController *deFacebookComposeViewController = [[DEFacebookComposeViewController alloc] init];
    [deFacebookComposeViewController setInitialText:[wallPost objectForKey:@"message"]];
    [deFacebookComposeViewController addURL:[wallPost objectForKey:@"link"]];
    [deFacebookComposeViewController setCompletionHandler:^(DEFacebookComposeViewControllerResult result) {
        switch (result) {
            case DEFacebookComposeViewControllerResultDone:
                [self.delegate psShareDidPublishPostOnFacebookWithSuccess:self];
                break;
            case DEFacebookComposeViewControllerResultCancelled:
                [self.delegate psShareDidNotPublishPostOnFacebook:self];
                break;
        }
    }];

    return deFacebookComposeViewController;
}

#pragma mark Mail methods

- (UIViewController *)mailShareComposeViewControllerForMessage:(NSString *)message withTitle:(NSString *)title{
	MFMailComposeViewController *emailComposeViewController = [[MFMailComposeViewController alloc] init];
    emailComposeViewController.mailComposeDelegate = self;
	[emailComposeViewController setSubject:title];
	[emailComposeViewController setMessageBody:message isHTML:NO];

    return emailComposeViewController;
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	switch (result) {
        case MFMailComposeResultSent:
            [self.delegate psShareDidSendMailWithSuccess:self];
            break;
        case MFMailComposeResultCancelled:
        case MFMailComposeResultSaved:
        case MFMailComposeResultFailed:
            [self.delegate psShareDidNotSendMail:self];
            break;
        default:
            break;
    }
}

#pragma mark - Facebook Session Methods

- (BOOL)facebookAuthenticated {
    return FBSession.activeSession.state == FBSessionStateOpen;
}

- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
    return [FBSession openActiveSessionWithPublishPermissions:@[ @"publish_actions" ]
                                              defaultAudience:FBSessionDefaultAudienceFriends
                                                 allowLoginUI:allowLoginUI
                                            completionHandler:^(FBSession *session,
                                                                FBSessionState state,
                                                                NSError *error) {

                                                [self sessionStateChanged:session state:state error:error];
                                            }];
}

- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState)state error:(NSError *)error {
    switch (state) {
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed: {
            if (error) {
                [FBSession.activeSession closeAndClearTokenInformation];
            }
            break;
        }
        default:
            break;
    }
}

- (void)publishWallPost:(NSDictionary *)wallPost {

    [FBRequestConnection startWithGraphPath:@"me/feed"
                                 parameters:wallPost
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error) {
                              if (error) {
                                  [self.delegate psShareDidNotPublishPostOnFacebook:self];
                              } else {
                                  [self.delegate psShareDidPublishPostOnFacebookWithSuccess:self];
                              }
                          }];
}

@end