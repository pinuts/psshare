//
//  DEFacebookComposeViewController.h
//  DEFacebookComposeViewController
//
//  Created by Felipe Vieira on 11/12/12.
//  Copyright (c) 2012 Pinuts Studios Cons. em T.I. Ltda. All rights reserved.
//

@class DEFacebookSheetCardView;
@class DEFacebookTextView;

@interface DEFacebookComposeViewController : UIViewController <UITextViewDelegate, UIAlertViewDelegate>

@property (retain, nonatomic) IBOutlet DEFacebookSheetCardView *cardView;
@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (retain, nonatomic) IBOutlet UIButton *cancelButton;
@property (retain, nonatomic) IBOutlet UIButton *sendButton;
@property (retain, nonatomic) IBOutlet UIView *cardHeaderLineView;
@property (retain, nonatomic) IBOutlet DEFacebookTextView *textView;
@property (retain, nonatomic) IBOutlet UIView *textViewContainer;
@property (retain, nonatomic) IBOutlet UIImageView *paperClipView;
@property (retain, nonatomic) IBOutlet UIImageView *attachment1FrameView;
@property (retain, nonatomic) IBOutlet UIImageView *attachment2FrameView;
@property (retain, nonatomic) IBOutlet UIImageView *attachment3FrameView;
@property (retain, nonatomic) IBOutlet UIImageView *attachment1ImageView;
@property (retain, nonatomic) IBOutlet UIImageView *attachment2ImageView;
@property (retain, nonatomic) IBOutlet UIImageView *attachment3ImageView;
@property (retain, nonatomic) IBOutlet UILabel *characterCountLabel;
@property (assign, nonatomic) IBOutlet UIImageView *navImage;

    // Public
- (IBAction)send;
- (IBAction)cancel;

enum DEFacebookComposeViewControllerResult {
    DEFacebookComposeViewControllerResultCancelled,
    DEFacebookComposeViewControllerResultDone
};


typedef enum DEFacebookComposeViewControllerResult DEFacebookComposeViewControllerResult;

    // Completion handler for DEFacebookComposeViewController
typedef void (^DEFacebookComposeViewControllerCompletionHandler)(DEFacebookComposeViewControllerResult result); 


    // Sets the initial text to be tweeted. Returns NO if the specified text will
    // not fit within the character space currently available, or if the sheet
    // has already been presented to the user.
- (BOOL)setInitialText:(NSString *)text;

    // Adds an image to the tweet. Returns NO if the additional image will not fit
    // within the character space currently available, or if the sheet has already
    // been presented to the user.
- (BOOL)addImage:(UIImage *)image;

    // Removes all images from the tweet. Returns NO and does not perform an operation
    // if the sheet has already been presented to the user. 
- (BOOL)removeAllImages;

    // Adds a URL to the tweet. Returns NO if the additional URL will not fit
    // within the character space currently available, or if the sheet has already
    // been presented to the user.
- (BOOL)addURL:(NSString *)url;

//    // Removes all URLs from the tweet. Returns NO and does not perform an operation
//    // if the sheet has already been presented to the user.
//- (BOOL)removeAllURLs;

    // Specify a block to be called when the user is finished. This block is not guaranteed
    // to be called on any particular thread.
@property (nonatomic, copy) DEFacebookComposeViewControllerCompletionHandler completionHandler;

@property (nonatomic, copy) NSDictionary *customParameters;

@end
