//
//  AppDelegate.h
//  Example
//
//  Created by Felipe Vieira on 11/30/12.
//  Copyright (c) 2012 Pinuts Studios Cons. em T.I. Ltda. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
