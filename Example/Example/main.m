//
//  main.m
//  Example
//
//  Created by Felipe Vieira on 11/30/12.
//  Copyright (c) 2012 Pinuts Studios Cons. em T.I. Ltda. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
