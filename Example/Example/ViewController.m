//
//  ViewController.m
//  Example
//
//  Created by Felipe Vieira on 11/30/12.
//  Copyright (c) 2012 Pinuts Studios Cons. em T.I. Ltda. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

- (IBAction)didClickOnMailButton:(UIButton *)sender;
- (IBAction)didClickOnFacebookButton:(UIButton *)sender;
- (IBAction)didClickOnTwitterButton:(UIButton *)sender;

@property (nonatomic, strong) PSShare *psShare;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

    self.psShare = [[PSShare alloc] init];
    [self.psShare setDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didClickOnMailButton:(UIButton *)sender {
    UIViewController *controller = [self.psShare mailShareComposeViewControllerForMessage:@"Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis." withTitle:@"Pra lá , depois divoltis porris."];
    [self presentViewController:controller animated:YES completion:nil];
}

- (IBAction)didClickOnFacebookButton:(UIButton *)sender {
    NSDictionary *wallPost = @{ @"link":@"http://mussumipsum.com/",
    @"picture":@"http://mussumipsum.com/images/mussum_bg.png",
    @"name":@"Manduma pindureta quium dia nois paga.",
    @"caption":@"Mussum Ipsum",
    @"message":@"Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis." };
    UIViewController *controller = [self.psShare facebookShareComposeViewControllerForWallPost:wallPost];
    if (controller) {
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (IBAction)didClickOnTwitterButton:(UIButton *)sender {
    UIViewController *controller = [self.psShare twitterShareComposeViewControllerForMessage:@"Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis."];
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark - PSShareDelegate
#pragma mark Mail delegate methods

- (void)psShareDidSendMailWithSuccess:(PSShare *)psShare {
    [self dismissViewControllerAnimated:YES completion:nil];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Mail"
                                                    message:@"Mail sent"
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil, nil];
    [alert show];
}

- (void)psShareDidNotSendMail:(PSShare *)psShare {
    [self dismissViewControllerAnimated:YES completion:nil];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Mail"
                                                    message:@"Mail was not sent"
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark Facebook delegate methods

- (void)psShareDidPublishPostOnFacebookWithSuccess:(PSShare *)psShare {
    [self dismissViewControllerAnimated:YES completion:nil];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook"
                                                    message:@"Post published on facebook"
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil, nil];
    [alert show];
}

- (void)psShareDidNotPublishPostOnFacebook:(PSShare *)psShare {
    [self dismissViewControllerAnimated:YES completion:nil];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook"
                                                    message:@"Post was not published on facebook"
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil, nil];
    [alert show];
}
#pragma mark Twitter delegate methods

-(void)psShareDidPostMessageOnTwitterWithSuccess:(PSShare *)psShare {
    [self dismissViewControllerAnimated:YES completion:nil];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter"
                                                    message:@"Message posted on twitter"
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil, nil];
    [alert show];
}

-(void)psShareDidNotPostMessageOnTwitter:(PSShare *)psShare {
    [self dismissViewControllerAnimated:YES completion:nil];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter"
                                                    message:@"Message was not posted on twitter"
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil, nil];
    [alert show];
}
@end
