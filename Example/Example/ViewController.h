//
//  ViewController.h
//  Example
//
//  Created by Felipe Vieira on 11/30/12.
//  Copyright (c) 2012 Pinuts Studios Cons. em T.I. Ltda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSShare.h"

@interface ViewController : UIViewController <PSShareDelegate>

@end
